package com.deven.sampleandroidarchitecture.network;

import com.deven.sampleandroidarchitecture.pojo.user.UserProfileResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Deven Singh on 4/12/2018.
 */

public interface ApiService {

    @GET("users/{user_name}")
    Call<ApiResponse<UserProfileResponse>> getUserProfile(@Path("user_name") String userName);

}
