package com.deven.sampleandroidarchitecture.data.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;


import com.deven.sampleandroidarchitecture.data.database.dao.AboutDao;
import com.deven.sampleandroidarchitecture.data.database.dao.UserDao;
import com.deven.sampleandroidarchitecture.data.database.entities.About;
import com.deven.sampleandroidarchitecture.data.database.entities.User;

import javax.inject.Singleton;

/**
 * Created by Deven Singh on 4/11/2018.
 */
@Singleton
@Database(entities = {About.class, User.class}, version = 1)
public abstract class SampleRoomDatabase extends RoomDatabase {

    public abstract AboutDao aboutDao();

    public abstract UserDao userDao();
}
