package com.deven.sampleandroidarchitecture.data.repository;


import com.deven.sampleandroidarchitecture.data.Task;

import java.util.HashMap;

import retrofit2.Call;

/**
 * Created by Deven Singh on 4/22/2018.
 */

public abstract class BaseRepository {

    public HashMap<Task, Call> callHashMap;

    public BaseRepository() {
        callHashMap = new HashMap<>();
    }

    public void cancelTask(Task taskId) {
        if (callHashMap != null && !callHashMap.isEmpty()) {
            callHashMap.get(taskId).cancel();
        }
    }

    public void cancelAllTasks() {
        if (callHashMap != null && !callHashMap.isEmpty()) {
            for (Task task : callHashMap.keySet()) {
                callHashMap.get(task).cancel();
            }
        }
    }
}
