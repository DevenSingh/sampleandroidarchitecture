package com.deven.sampleandroidarchitecture.data.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;

import com.deven.sampleandroidarchitecture.data.Task;
import com.deven.sampleandroidarchitecture.network.ApiResponse;
import com.deven.sampleandroidarchitecture.network.ApiService;
import com.deven.sampleandroidarchitecture.pojo.common.Resource;
import com.deven.sampleandroidarchitecture.pojo.user.UserProfileResponse;
import com.deven.sampleandroidarchitecture.utils.Logger;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Deven Singh on 4/11/2018.
 */
public class UserRepository extends BaseRepository {

    private final ApiService apiService;

    public UserRepository(ApiService apiService) {
        this.apiService = apiService;
    }

    public LiveData<Resource<UserProfileResponse>> getUserProfile(String name) {
        MediatorLiveData<Resource<UserProfileResponse>> result = new MediatorLiveData<>();
        result.setValue(Resource.loading(null));
        Call<ApiResponse<UserProfileResponse>> fetchUserProfile = apiService.getUserProfile(name);
        if (callHashMap == null) callHashMap = new HashMap<>();
        callHashMap.put(Task.GET_USER_PROFILE, fetchUserProfile);
        fetchUserProfile.enqueue(new Callback<ApiResponse<UserProfileResponse>>() {
            @Override
            public void onResponse(Call<ApiResponse<UserProfileResponse>> call, Response<ApiResponse<UserProfileResponse>> response) {
                callHashMap.remove(Task.GET_USER_PROFILE);
                if (response.isSuccessful()) {
                    ApiResponse<UserProfileResponse> baseResponse = response.body();
                    if (baseResponse.isSuccessful()) {
                        result.setValue(Resource.success(baseResponse.body));
                    } else {
                        result.setValue(Resource.error(baseResponse.errorMessage));
                    }
                } else {
                    String message = null;
                    try {
                        message = response.errorBody().string();
                    } catch (IOException e) {
                        Logger.debug("createNewAccount(requestData): ", "IOException occurs: " + e);
                    }
                    result.setValue(Resource.error(message == null ? "Something went wrong. Please try again" : message));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<UserProfileResponse>> call, Throwable t) {
                callHashMap.remove(Task.GET_USER_PROFILE);
                if (call.isCanceled()) return;
                if (t instanceof ConnectException) {
                    result.setValue(Resource.error("No Network Connection."));
                    return;
                }
                if (t instanceof SocketTimeoutException) {
                    result.setValue(Resource.error("Connection Timeout!"));
                    return;
                }
                result.setValue(Resource.error("Something went wrong. Please try again."));
            }
        });
        return result;
    }
}
