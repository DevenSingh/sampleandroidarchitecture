package com.deven.sampleandroidarchitecture.data;

/**
 * Created by Deven Singh on 5/17/2018.
 */

public enum Task {

    GET_USER_PROFILE,SIGN_IN_TASK,SIGN_UP_TASK
}
