package com.deven.sampleandroidarchitecture.di.component;

import com.deven.sampleandroidarchitecture.DevApplication;
import com.deven.sampleandroidarchitecture.di.builder.ActivityBuilder;
import com.deven.sampleandroidarchitecture.di.module.AppModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by Deven Singh on 4/9/2018.
 */
@Singleton
@Component(modules = {AndroidSupportInjectionModule.class, AppModule.class, ActivityBuilder.class})
public interface AppComponent extends AndroidInjector<DevApplication> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<DevApplication> {
    }
}