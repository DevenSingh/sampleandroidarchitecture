package com.deven.sampleandroidarchitecture.di.builder;

import com.deven.sampleandroidarchitecture.ui.user.MainActivity;
import com.deven.sampleandroidarchitecture.ui.user.MainFragmentBuilder;
import com.deven.sampleandroidarchitecture.ui.user.UserModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Deven Singh on 4/14/2018.
 */
@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = {MainFragmentBuilder.class, UserModule.class})
    abstract MainActivity bindMainActivity();

}
