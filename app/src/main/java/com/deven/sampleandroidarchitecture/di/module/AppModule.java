package com.deven.sampleandroidarchitecture.di.module;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;

import com.deven.sampleandroidarchitecture.BuildConfig;
import com.deven.sampleandroidarchitecture.DevApplication;
import com.deven.sampleandroidarchitecture.data.database.SampleRoomDatabase;
import com.deven.sampleandroidarchitecture.di.ApplicationContext;
import com.deven.sampleandroidarchitecture.di.DatabaseInfo;
import com.deven.sampleandroidarchitecture.di.PreferenceInfo;
import com.deven.sampleandroidarchitecture.network.ApiService;
import com.deven.sampleandroidarchitecture.utils.AppConstants;
import com.deven.sampleandroidarchitecture.utils.LiveDataCallAdapterFactory;
import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Deven Singh on 4/9/2018.
 */
@Module
public class AppModule {

    @Provides
    Application provideApplication(DevApplication application) {
        return application;
    }

    @Provides
    @ApplicationContext
    Context provideContext(Application application) {
        return application;
    }

    @Provides
    @Singleton
    SampleRoomDatabase provideDatabase(@ApplicationContext Context context, @DatabaseInfo String databaseName) {
        return Room.databaseBuilder(context, SampleRoomDatabase.class, databaseName).build();
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return AppConstants.DATABASE_NAME;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPref(@ApplicationContext Context context, @PreferenceInfo String prefName) {
        return context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    ApiService provideApiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit
                .Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .client(okHttpClient).build();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(OkHttpClient.Builder builder, HttpLoggingInterceptor interceptor) {
        return (BuildConfig.DEBUG && !builder.interceptors().contains(interceptor))
                ? builder.addInterceptor(interceptor).build()
                : builder.build();
    }

    @Provides
    @Singleton
    OkHttpClient.Builder provideOkHttpClientBuilder() {
        return new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS);
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        return new HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    @Provides
    AssetManager provideAssetManager(@ApplicationContext Context context) {
        return context.getAssets();
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new Gson();
    }
}
