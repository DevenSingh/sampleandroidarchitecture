package com.deven.sampleandroidarchitecture.utils;

/**
 * Created by Deven Singh on 4/11/2018.
 */

public interface AppConstants {

    String DATABASE_NAME = "sampleApp.db";
    String PREF_NAME = "sampleApp_pref";

    interface Keys {
        String EMAIL_ID = "emailId";
        String PASSWORD = "password";
        String DEVICE_ID = "deviceId";
        String ADDITIONAL_INFO = "additionalInfo";
        String VERSION_SDK = "versionSDK";
        String DEVICE = "device";
        String MODEL = "model";
        String MOBILE_NUMBER="mobileNumber";
        String PRODUCT = "product";
        String BRAND = "brand";
        String DEVICE_NAME = "deviceName";
        String OS_VERSION_ID = "osVersionID";
        String IS_NEW_USER="isNewUser";
    }

    interface ToolbarTitle {
        String APP_NAME="Sample App";
        String USER_PROFILE="Profile";
    }
}
