package com.deven.sampleandroidarchitecture.utils;

import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Deven Singh on 4/11/2018.
 */
@Singleton
public class SharedPrefs {

    interface Key {
        String IS_FIRST_TIME = "isFirstTime";
    }

    private SharedPreferences mSharedPreferences;

    @Inject
    public SharedPrefs(SharedPreferences sharedPreferences) {
        mSharedPreferences = sharedPreferences;
    }

    public boolean isFirstTime() {
        return mSharedPreferences.getBoolean(Key.IS_FIRST_TIME, true);
    }

    public void setFirstTime(boolean firstTime) {
        mSharedPreferences.edit().putBoolean(Key.IS_FIRST_TIME, firstTime);
    }
}
