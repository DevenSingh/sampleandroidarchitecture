package com.deven.sampleandroidarchitecture.utils;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;


import com.deven.sampleandroidarchitecture.di.ApplicationContext;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by Deven Singh on 5/18/2018.
 */

public class AppUtils {

    private final Context context;

    @Inject
    public AppUtils(@ApplicationContext Context context){
        this.context=context;
    }

    public String getAndroidId() {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    /**
     * method to get device details
     * @return {@link Map}
     */
    public Map<String, Object> getDeviceDetails() {
        Map<String, Object> deviceDetails = new HashMap<>();
        deviceDetails.put(AppConstants.Keys.VERSION_SDK, Build.VERSION.SDK_INT);
        deviceDetails.put(AppConstants.Keys.DEVICE, Build.DEVICE);
        deviceDetails.put(AppConstants.Keys.MODEL, Build.MODEL);
        deviceDetails.put(AppConstants.Keys.MODEL, Build.PRODUCT);
        deviceDetails.put(AppConstants.Keys.BRAND, Build.BRAND);
        deviceDetails.put(AppConstants.Keys.DEVICE_NAME,  getDeviceName());
        deviceDetails.put(AppConstants.Keys.OS_VERSION_ID, getOsVersion());
        return deviceDetails;
    }

    /**
     * To get Manufacturer and Model name of Device
     */
    public String getDeviceName() {
        String deviceMfg = Build.MANUFACTURER;
        String deviceModel = Build.MODEL;
        if (deviceModel.startsWith(deviceMfg)) {
            return deviceModel.toUpperCase();
        }
        return deviceMfg.toUpperCase() + " " + deviceModel;
    }

    /**
     * To get OS version
     */
    public Integer getOsVersion() {
        return Build.VERSION.SDK_INT;
    }
}
