package com.deven.sampleandroidarchitecture;

import com.deven.sampleandroidarchitecture.di.component.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

/**
 * Created by Deven Singh on 5/27/2018.
 */
public class DevApplication extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DevApplication> applicationInjector() {
        return DaggerAppComponent.builder().create(this);
    }

}