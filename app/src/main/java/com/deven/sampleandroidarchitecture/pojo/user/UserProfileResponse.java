package com.deven.sampleandroidarchitecture.pojo.user;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Deven Singh on 5/27/2018.
 */
@Entity(tableName = "user_table")
public class UserProfileResponse {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "uid")
    long userId;
    @ColumnInfo(name = "login_id")
    private String login;
    @ColumnInfo(name = "user_id")
    private Integer id;
    @ColumnInfo(name = "avatarUrl")
    private String avatarUrl;
    @ColumnInfo(name = "gravatarId")
    private String gravatarId;
    @ColumnInfo(name = "url")
    private String url;
    @ColumnInfo(name = "htmlUrl")
    private String htmlUrl;
    @ColumnInfo(name = "followersUrl")
    private String followersUrl;
    @ColumnInfo(name = "followingUrl")
    private String followingUrl;
    @ColumnInfo(name = "gistsUrl")
    private String gistsUrl;
    @ColumnInfo(name = "starredUrl")
    private String starredUrl;
    @ColumnInfo(name = "subscriptionsUrl")
    private String subscriptionsUrl;
    @ColumnInfo(name = "organizationsUrl")
    private String organizationsUrl;
    @ColumnInfo(name = "reposUrl")
    private String reposUrl;
    @ColumnInfo(name = "eventsUrl")
    private String eventsUrl;
    @ColumnInfo(name = "receivedEventsUrl")
    private String receivedEventsUrl;
    @ColumnInfo(name = "type")
    private String type;
    @ColumnInfo(name = "siteAdmin")
    private Boolean siteAdmin;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "company")
    private String company;
    @ColumnInfo(name = "blog")
    private String blog;
    @ColumnInfo(name = "location")
    private String location;
    @ColumnInfo(name = "email")
    private String email;
    @ColumnInfo(name = "hireable")
    private Object hireable;
    @ColumnInfo(name = "bio")
    private Object bio;
    @ColumnInfo(name = "publicRepos")
    private Integer publicRepos;
    @ColumnInfo(name = "publicGists")
    private Integer publicGists;
    @ColumnInfo(name = "followers")
    private Integer followers;
    @ColumnInfo(name = "following")
    private Integer following;
    @ColumnInfo(name = "createdAt")
    private String createdAt;
    @ColumnInfo(name = "updatedAt")
    private String updatedAt;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getGravatarId() {
        return gravatarId;
    }

    public void setGravatarId(String gravatarId) {
        this.gravatarId = gravatarId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public String getFollowersUrl() {
        return followersUrl;
    }

    public void setFollowersUrl(String followersUrl) {
        this.followersUrl = followersUrl;
    }

    public String getFollowingUrl() {
        return followingUrl;
    }

    public void setFollowingUrl(String followingUrl) {
        this.followingUrl = followingUrl;
    }

    public String getGistsUrl() {
        return gistsUrl;
    }

    public void setGistsUrl(String gistsUrl) {
        this.gistsUrl = gistsUrl;
    }

    public String getStarredUrl() {
        return starredUrl;
    }

    public void setStarredUrl(String starredUrl) {
        this.starredUrl = starredUrl;
    }

    public String getSubscriptionsUrl() {
        return subscriptionsUrl;
    }

    public void setSubscriptionsUrl(String subscriptionsUrl) {
        this.subscriptionsUrl = subscriptionsUrl;
    }

    public String getOrganizationsUrl() {
        return organizationsUrl;
    }

    public void setOrganizationsUrl(String organizationsUrl) {
        this.organizationsUrl = organizationsUrl;
    }

    public String getReposUrl() {
        return reposUrl;
    }

    public void setReposUrl(String reposUrl) {
        this.reposUrl = reposUrl;
    }

    public String getEventsUrl() {
        return eventsUrl;
    }

    public void setEventsUrl(String eventsUrl) {
        this.eventsUrl = eventsUrl;
    }

    public String getReceivedEventsUrl() {
        return receivedEventsUrl;
    }

    public void setReceivedEventsUrl(String receivedEventsUrl) {
        this.receivedEventsUrl = receivedEventsUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getSiteAdmin() {
        return siteAdmin;
    }

    public void setSiteAdmin(Boolean siteAdmin) {
        this.siteAdmin = siteAdmin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBlog() {
        return blog;
    }

    public void setBlog(String blog) {
        this.blog = blog;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getHireable() {
        return hireable;
    }

    public void setHireable(Object hireable) {
        this.hireable = hireable;
    }

    public Object getBio() {
        return bio;
    }

    public void setBio(Object bio) {
        this.bio = bio;
    }

    public Integer getPublicRepos() {
        return publicRepos;
    }

    public void setPublicRepos(Integer publicRepos) {
        this.publicRepos = publicRepos;
    }

    public Integer getPublicGists() {
        return publicGists;
    }

    public void setPublicGists(Integer publicGists) {
        this.publicGists = publicGists;
    }

    public Integer getFollowers() {
        return followers;
    }

    public void setFollowers(Integer followers) {
        this.followers = followers;
    }

    public Integer getFollowing() {
        return following;
    }

    public void setFollowing(Integer following) {
        this.following = following;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
