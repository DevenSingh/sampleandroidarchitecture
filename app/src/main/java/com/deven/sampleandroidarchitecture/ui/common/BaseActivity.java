package com.deven.sampleandroidarchitecture.ui.common;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.deven.sampleandroidarchitecture.R;
import com.deven.sampleandroidarchitecture.utils.AppConstants;

import java.util.ArrayList;

import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * Created by Deven Singh on 4/12/2018.
 */

public abstract class BaseActivity extends DaggerAppCompatActivity {

    protected abstract
    @LayoutRes
    int getLayoutId();

    protected abstract
    @IdRes
    int getBaseContainerId();

    protected int baseContainerId = R.id.base_main_container;
    protected int baseActivityLayout = R.layout.activity_base;
    protected Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        setUpToolbar();
    }

    private void setUpToolbar() {
        if (getBaseContainerId() != baseContainerId) return;
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(AppConstants.ToolbarTitle.APP_NAME);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    public void replaceFragment(Fragment fragment, boolean isAddToBackStack, String tag) {
        replaceFragment(fragment, isAddToBackStack, tag, false, null, null);
    }

    public void replaceFragment(Fragment fragment, boolean isAddToBackStack, String tag,
                                boolean isAddSharedElement, ArrayList<View> sharedElementView, ArrayList<String> transitionName) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if (isAddSharedElement) {
            int count = sharedElementView.size();
            for (int i = 0; i < count; i++) {
                transaction.addSharedElement(sharedElementView.get(i), transitionName.get(i));
            }
        }
        transaction.replace(getBaseContainerId(), fragment, tag);
        if (isAddToBackStack)
            transaction.addToBackStack(tag);
        transaction.commit();
    }

    public void replaceFragmentWithPrevious(Fragment fragment, boolean isAddToBackStack, String tag) {
        replaceFragmentWithPrevious(fragment, isAddToBackStack, tag, false, null, null);
    }

    public void replaceFragmentWithPrevious(Fragment fragment, boolean isAddToBackStack, String tag,
                                            boolean isAddSharedElement, ArrayList<View> sharedElementView,
                                            ArrayList<String> transitionName) {
        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(tag, 0);
        if (!fragmentPopped) {
            replaceFragment(fragment, isAddToBackStack, tag, isAddSharedElement, sharedElementView, transitionName);
        }
    }

    public void showSnackBar(String message, boolean isError) {
        View view = findViewById(android.R.id.content);
        try {
            Snackbar snackbar;
            snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(isError ? Color.RED : ContextCompat.getColor(this, R.color.color_accent));
            TextView textView = snackBarView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        } catch (Exception e) {
        }
    }

    public void showToast(String toastText) {
        showToast(toastText, false);
    }

    public void showToast(String toastText, boolean isLengthLong) {
        if (isLengthLong)
            Toast.makeText(this, toastText, Toast.LENGTH_LONG).show();
        else
            Toast.makeText(this, toastText, Toast.LENGTH_SHORT).show();
    }
}
