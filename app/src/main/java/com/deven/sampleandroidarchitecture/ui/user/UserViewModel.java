package com.deven.sampleandroidarchitecture.ui.user;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.deven.sampleandroidarchitecture.data.repository.UserRepository;
import com.deven.sampleandroidarchitecture.pojo.common.Resource;
import com.deven.sampleandroidarchitecture.pojo.user.UserProfileResponse;
import com.deven.sampleandroidarchitecture.utils.AbsentLiveData;

/**
 * Created by Deven Singh on 4/20/2018.
 */
public class UserViewModel extends ViewModel {

    private final MutableLiveData<String> inputData = new MutableLiveData<>();
    private LiveData<Resource<UserProfileResponse>> userProfileResponse;


    private UserRepository repository;

    private String lastProfileRequest;

    public UserViewModel(UserRepository repository) {
        this.repository = repository;
    }

    public LiveData<Resource<UserProfileResponse>> getUserProfile() {
        userProfileResponse = Transformations.switchMap(inputData, input -> {
            if (input.isEmpty() || input.equals(lastProfileRequest)) {
                lastProfileRequest = input;
                return AbsentLiveData.create();
            }
            return repository.getUserProfile(input);
        });
        return userProfileResponse;
    }

    public void setName(String name) {
        inputData.setValue(name);
    }
}
