package com.deven.sampleandroidarchitecture.ui.user;

import com.deven.sampleandroidarchitecture.ViewModelProviderFactory;
import com.deven.sampleandroidarchitecture.data.repository.UserRepository;
import com.deven.sampleandroidarchitecture.network.ApiService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Deven Singh on 4/13/2018.
 */
@Module
public class UserModule {

    @Provides
    UserRepository provideUserRepository(ApiService apiService) {
        return new UserRepository(apiService);
    }

    @Provides
    UserViewModel provideUserViewModel(UserRepository repository) {
        return new UserViewModel(repository);
    }

    @Provides
    ViewModelProviderFactory provideViewModelProviderFactory(UserViewModel userViewModel) {
        return new ViewModelProviderFactory(userViewModel);
    }
}
