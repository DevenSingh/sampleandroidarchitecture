package com.deven.sampleandroidarchitecture.ui.user;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.deven.sampleandroidarchitecture.ViewModelProviderFactory;
import com.deven.sampleandroidarchitecture.data.database.SampleRoomDatabase;
import com.deven.sampleandroidarchitecture.pojo.common.Resource;
import com.deven.sampleandroidarchitecture.pojo.user.UserProfileResponse;
import com.deven.sampleandroidarchitecture.ui.common.BaseFragment;
import com.deven.sampleandroidarchitecture.utils.AppConstants;
import com.deven.sampleandroidarchitecture.utils.SharedPrefs;

import javax.inject.Inject;

/**
 * Created by Deven Singh on 5/27/2018.
 */
public class UserProfileFragment extends BaseFragment<UserViewModel> {

    public final static String TAG = "UserProfileFragment";

    @Inject
    SharedPrefs sharedPrefs;
    @Inject
    SampleRoomDatabase database;
    @Inject
    ViewModelProviderFactory viewModelProviderFactory;

    private UserViewModel mViewModel;

    public static UserProfileFragment newInstance() {
        return new UserProfileFragment();
    }

    @Override
    protected int getFragLayoutId() {
        return 0;
    }

    @Override
    protected UserViewModel getViewModel() {
        return mViewModel = ViewModelProviders.of(this, viewModelProviderFactory).get(UserViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setToolbarTitle(AppConstants.ToolbarTitle.USER_PROFILE);
        mViewModel.getUserProfile().observe(this, observer);
        initComponents();
    }

    private void initComponents() {
         if (sharedPrefs.isFirstTime()){
             mViewModel.setName("devensingh1727");
         }else {
//             database.userDao().getUser().observe();
         }
    }

    Observer<Resource<UserProfileResponse>> observer = resource -> {
        switch (resource.status) {
            case LOADING:
                // TODO: 5/27/2018 show progressbar
                break;
            case SUCCESS:
                showSnackBar("result updated");
                break;
            case ERROR:
                showSnackBar("error", true);
                break;
        }
    };
}
