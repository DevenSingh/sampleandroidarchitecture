package com.deven.sampleandroidarchitecture.ui.user;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Deven Singh on 5/27/2018.
 */
@Module
public abstract class MainFragmentBuilder {

    @ContributesAndroidInjector
    abstract UserProfileFragment bindUserProfileFragment();
}