package com.deven.sampleandroidarchitecture.ui.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.deven.sampleandroidarchitecture.R;
import com.deven.sampleandroidarchitecture.ui.common.BaseActivity;


/**
 * Created by Deven Singh on 4/9/2018.
 */
public class MainActivity extends BaseActivity {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_base;
    }

    @Override
    protected int getBaseContainerId() {
        return R.id.base_main_container;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceFragment(UserProfileFragment.newInstance(), true, UserProfileFragment.TAG);
    }

    @Override
    public void onBackPressed() {
        int index = getSupportFragmentManager().getBackStackEntryCount() - 1;
        FragmentManager.BackStackEntry backEntry = getSupportFragmentManager().getBackStackEntryAt(index);
        String tag = backEntry.getName();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (index == 0 || fragment instanceof UserProfileFragment) {
            finish();
        } else {
            super.onBackPressed();
        }
    }
}
